package com.webregistro.actions;

import com.opensymphony.xwork2.ActionSupport;
import com.webregistro.daos.GeneralEstDao;
import com.webregistro.to.RespuestaTo;
import com.webregistro.to.EstudianteTo;
import com.webregistro.to.EvaluacionTo;
import com.webregistro.to.PreguntaTo;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.interceptor.ServletRequestAware;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: new user
 * Date: 25-nov-2011
 * Time: 15:57:19
 * To change this template use File | Settings | File Templates.
 */
@SuppressWarnings("serial")
public class GeneralEstAction extends ActionSupport implements SessionAware, ServletRequestAware {
    private String usuario;
    private String password;

    private String cedula;
    private String nombre;
    private String semestre;
    @SuppressWarnings("unchecked")
    private Map sesion;
    private HttpServletRequest request;

    private List<PreguntaTo> listaJSPPreguntasTo;
    private List<RespuestaTo> listaJSPRespuestasTo;
    private int notaGeneral;

    private Boolean activar;
    public String inicioEst()throws Exception{
        if((this.getSesion().get("CedulaProfesor")==null)||
                ((String)this.getSesion().get("CedulaEstudiante")).equals("")){
            return "sesion_expirada";
        }
        this.setUsuario("");
        this.setPassword("");
        this.setCedula("");
        this.setNombre("");
        return SUCCESS;
    }

    public String iniciarLoginEst()throws Exception{
        this.setUsuario("");
        this.setPassword("");
        return SUCCESS;
    }

    @SuppressWarnings("unchecked")
    public String login() throws Exception {
        GeneralEstDao generalEstDao = new GeneralEstDao();
        System.out.println("Usuario: " + this.getUsuario());
        System.out.println("Password: " + this.getPassword());
        List<EstudianteTo> listEstudianteTo = null;
        try {
            listEstudianteTo = generalEstDao.consultarEstudiantes(this.getUsuario(),this.getPassword());
            if(listEstudianteTo!=null && listEstudianteTo.size()>0){
                this.setCedula(listEstudianteTo.get(0).getCedula());
                this.setNombre(listEstudianteTo.get(0).getNombre());
                this.setSemestre(listEstudianteTo.get(0).getSemestre());
                this.getSesion().put("CedulaEstudiante",this.getCedula());
                this.getSesion().put("NombreEstudiante",this.getNombre());
                this.getSesion().put("SemestreEstudiante",this.getSemestre());
            }else{
                addActionMessage("No se pudo obtener los datos del usuario...");
                return INPUT;
            }
        } catch (Exception e) {
            e.printStackTrace();
            addActionMessage("Ocurrio un error al intentar acceder a la aplicaccion...\n" + e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }

    public String iniciarIngresarEstudiante()throws Exception{
//        if((this.getSesion().get("CedulaProfesor")==null)||
//                ((String)this.getSesion().get("CedulaEstudiante")).equals("")){
//            return "sesion_expirada";
//        }
        this.setCedula("");
        this.setNombre("");
        this.setSemestre("");
        return SUCCESS;
    }
    @SuppressWarnings("unchecked")

    public String ingresarEstudiante(){
        GeneralEstDao generalEstDao = new GeneralEstDao();
        List<EstudianteTo> listEstudianteTo = null;
//        if((this.getSesion().get("CedulaProfesor")==null)||
//                ((String)this.getSesion().get("CedulaEstudiante")).equals("")){
//            return "sesion_expirada";
//        }
        System.out.println("Cedula: " + this.getCedula());
        System.out.println("Nombre: " + this.getNombre());
        System.out.println("Semestre: " + this.getSemestre());
        System.out.println("Password: " + this.getPassword());
        try {
            listEstudianteTo = generalEstDao.consultarEstudiantes(this.getCedula(),"");
            if(listEstudianteTo==null || listEstudianteTo.size()==0){
                generalEstDao.insertarEstudiante(this.getCedula(),this.getNombre(),this.getSemestre(),this.getPassword());
                this.getSesion().put("CedulaEstudiante",this.getCedula());
                this.getSesion().put("NombreEstudiante",this.getNombre());
                this.getSesion().put("SemestreEstudiante",this.getSemestre());
            }else{
                addActionMessage("Este usuario ya esta registrado...");
                return INPUT;
            }
        } catch (Exception e) {
            e.printStackTrace();
            addActionMessage("Ocurrio un error al intentar ingresar los datos suminstrados...\n" + e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }
    public String iniciarModificarEstudiante()throws Exception{
        if((this.getSesion().get("CedulaProfesor")==null)||
                ((String)this.getSesion().get("CedulaEstudiante")).equals("")){
            return "sesion_expirada";
        }
        this.setCedula("");
        this.setNombre("");
        this.setSemestre("");
        return SUCCESS;
    }

    @SuppressWarnings("unchecked")
    public String modificarEstudiante(){
        GeneralEstDao generalEstDao = new GeneralEstDao();
        if((this.getSesion().get("CedulaProfesor")==null)||
                ((String)this.getSesion().get("CedulaEstudiante")).equals("")){
            return "sesion_expirada";
        }
        String cedulaActualEstudiante = (String)this.getSesion().get("CedulaEstudiante");
        System.out.println("Cedula: " + this.getCedula());
        System.out.println("Nombre: " + this.getNombre());
        System.out.println("Semestre: " + this.getSemestre());
        try {
            generalEstDao.modificarEstudiante(cedulaActualEstudiante,this.getCedula(),this.getNombre(),this.getSemestre(), this.getPassword());
            this.getSesion().put("CedulaEstudiante",this.getCedula());
            this.getSesion().put("NombreEstudiante",this.getNombre());
            this.getSesion().put("SemestreEstudiante",this.getSemestre());
        } catch (Exception e) {
            e.printStackTrace();
            addActionMessage("Ocurrio un error al intentar ingresar los datos suminstrados...\n" + e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }

    public String consultarEstudiante() throws Exception {
        if((this.getSesion().get("CedulaProfesor")==null)||
                ((String)this.getSesion().get("CedulaEstudiante")).equals("")){
            return "sesion_expirada";
        }
        System.out.println("Cedula: " + (String)this.getSesion().get("CedulaEstudiante"));
        System.out.println("Nombre: " + (String)this.getSesion().get("NombreEstudiante"));
        System.out.println("Nombre: " + (String)this.getSesion().get("SemestreEstudiante"));

        this.setCedula((String)this.getSesion().get("CedulaEstudiante"));
        this.setNombre((String)this.getSesion().get("NombreEstudiante"));
        this.setSemestre((String)this.getSesion().get("SemestreEstudiante"));
        return SUCCESS;
    }
    @SuppressWarnings("unchecked")
    public String logOut(){
        this.getSesion().put("CedulaEstudiante","");
        this.getSesion().put("NombreEstudiante","");
        this.getSesion().put("SemestreEstudiante","");
        return SUCCESS;
    }
    public String iniciarPresentarEvaluacion()throws Exception{
        if((this.getSesion().get("CedulaProfesor")==null)||
                ((String)this.getSesion().get("CedulaEstudiante")).equals("")){
            return "sesion_expirada";
        }
        this.setCedula("");
        this.setNombre("");
        this.setSemestre("");
        this.getSesion().remove("Evaluacion");
        this.setActivar(false);
        return SUCCESS;
    }
    @SuppressWarnings("unchecked")
    public String cargarEvaluacion(){
        GeneralEstDao generalEstDao = new GeneralEstDao();
        List<EvaluacionTo> listEvaluacionTo = null;
        List<PreguntaTo> listPreguntaTo = null;
        EvaluacionTo evaluacionTo = null;
        if((this.getSesion().get("CedulaProfesor")==null)||
                ((String)this.getSesion().get("CedulaEstudiante")).equals("")){
            return "sesion_expirada";
        }
        System.out.println("Cedula: " + this.getCedula());
        System.out.println("Nombre: " + this.getNombre());
        System.out.println("Semestre: " + this.getSemestre());
        System.out.println("Password: " + this.getPassword());
        try {
            listEvaluacionTo = generalEstDao.consultarEvaluacion(this.getRequest().getParameter("codigo"));
            if(listEvaluacionTo!=null && listEvaluacionTo.size()>0){
                evaluacionTo = listEvaluacionTo.get(0);
                listPreguntaTo = generalEstDao.consultarPreguntasEvaluacion(this.getRequest().getParameter("codigo"));
                if(listEvaluacionTo!=null && listEvaluacionTo.size()>0){
                    evaluacionTo.setPreguntas(listPreguntaTo);
                    this.getSesion().put("Evaluacion",evaluacionTo);
                    this.setListaJSPPreguntasTo(listPreguntaTo);
                }
                this.setActivar(true);
            }else{
                addActionMessage("Esta evaluacion no existe...");
                return INPUT;
            }
        } catch (Exception e) {
            e.printStackTrace();
            addActionMessage("Ocurrio un error al intentar cargar los datos...\n" + e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }
    public String presentarEvaluacion(){
        if((this.getSesion().get("CedulaProfesor")==null)||
                ((String)this.getSesion().get("CedulaEstudiante")).equals("")){
            return "sesion_expirada";
        }
        EvaluacionTo evaluacionTo = (EvaluacionTo) this.getSesion().get("Evaluacion");
        GeneralEstDao generalEstDao = new GeneralEstDao();
        RespuestaTo respuestaTo = null;
        List<RespuestaTo> listRespuestaTos = new ArrayList<RespuestaTo>();
        try {
            for(int ind=1 ; ind<=10 ; ind++){
                respuestaTo = new RespuestaTo();
                respuestaTo.setId_estudiante((String)this.getSesion().get("CedulaEstudiante"));
                respuestaTo.setId_evaluacion(evaluacionTo.getCodigo());
                respuestaTo.setId_pregunta(evaluacionTo.getPreguntas().get(ind-1).getIdPregunta());
                respuestaTo.setNumero_respuesta(Integer.parseInt(this.getRequest().getParameter("resp"+ ind + "Preg" + ind)));
                if(respuestaTo.getNumero_respuesta()==evaluacionTo.getPreguntas().get(ind-1).getResCorrecta())
                    respuestaTo.setNotaPrg(2);
                else
                    respuestaTo.setNotaPrg(0);
                listRespuestaTos.add(respuestaTo);
            }
            evaluacionTo.setRespuestas(listRespuestaTos);
            generalEstDao.insertarEvaluacion(evaluacionTo);
            this.getSesion().remove("Evaluacion");
        }catch (Exception e) {
            e.printStackTrace();
            addActionMessage("Ocurrio un error al intentar registrar los datos...\n" + e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }

    public String iniciarConsultarEvaluacion()throws Exception{
        if((this.getSesion().get("CedulaProfesor")==null)||
                ((String)this.getSesion().get("CedulaEstudiante")).equals("")){
            return "sesion_expirada";
        }
        this.setCedula("");
        this.setNombre("");
        this.setSemestre("");
        this.setActivar(false);
        return SUCCESS;
    }

    public String consultarEvaluacion(){
        List<RespuestaTo> listRespuestaTo = null;
        GeneralEstDao generalEstDao = new GeneralEstDao();
        if((this.getSesion().get("CedulaProfesor")==null)||
                ((String)this.getSesion().get("CedulaEstudiante")).equals("")){
            return "sesion_expirada";
        }
        try{
            listRespuestaTo = generalEstDao.consultarRespuestasEvaluacion((String)this.getRequest().getParameter("codigo"),(String)this.getSesion().get("CedulaEstudiante"));
            this.setListaJSPRespuestasTo(listRespuestaTo);
            this.setCedula((String)this.getSesion().get("CedulaEstudiante"));
            this.setNombre((String)this.getSesion().get("NombreEstudiante"));
            this.setSemestre((String)this.getSesion().get("SemestreEstudiante"));
            this.setNotaGeneral(0);
            for(int ind = 0 ; ind<listRespuestaTo.size() ; ind++)
                this.setNotaGeneral(this.getNotaGeneral()+listRespuestaTo.get(ind).getNotaPrg());
            this.setActivar(true);
        }catch (Exception e) {
            e.printStackTrace();
            addActionMessage("Ocurrio un error al intentar cargar los datos...\n" + e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }

    public List<PreguntaTo> getListaJSPPreguntasTo() {
        return listaJSPPreguntasTo;
    }

    public void setListaJSPPreguntasTo(List<PreguntaTo> listaJSPPreguntasTo) {
        this.listaJSPPreguntasTo = listaJSPPreguntasTo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }
    @SuppressWarnings("unchecked")
    public Map getSesion() {
        return sesion;
    }

    public HttpServletRequest getRequestAction() {
        return request;
    }

    public void setRequestAction(HttpServletRequest request) {
        this.request = request;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @SuppressWarnings("unchecked")
    public void setSession(Map s) {
        this.sesion = s;
    }

    public void setServletRequest(HttpServletRequest request) {
        this.request = request;
    }

    public Boolean isActivar() {
        return activar;
    }

    public Boolean getActivar(){
        return this.activar;
    }

    public void setActivar(Boolean activar) {
        this.activar = activar;
    }

    public List<RespuestaTo> getListaJSPRespuestasTo() {
        return listaJSPRespuestasTo;
    }

    public void setListaJSPRespuestasTo(List<RespuestaTo> listaJSPRespuestasTo) {
        this.listaJSPRespuestasTo = listaJSPRespuestasTo;
    }

    public int getNotaGeneral() {
        return notaGeneral;
    }

    public void setNotaGeneral(int notaGeneral) {
        this.notaGeneral = notaGeneral;
    }
}