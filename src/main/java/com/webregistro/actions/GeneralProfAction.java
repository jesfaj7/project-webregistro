package com.webregistro.actions;

import com.opensymphony.xwork2.ActionSupport;
import com.webregistro.daos.GeneralEstDao;
import com.webregistro.daos.GeneralProfDao;
import com.webregistro.to.*;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.interceptor.ServletRequestAware;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: new user
 * Date: 25-nov-2011
 * Time: 15:57:19
 * To change this template use File | Settings | File Templates.
 */
@SuppressWarnings("serial")
public class GeneralProfAction extends ActionSupport implements SessionAware, ServletRequestAware {
    private String usuario;
    private String password;

    private String cedula;
    private String nombre;
    private String semestre;

    private String codigo;

    @SuppressWarnings("unchecked")
    private Map sesion;
    private HttpServletRequest request;

    private List<RespuestaTo> listaJSPRespuestasTo;
    private int notaGeneral;
    private Boolean activar;


    public String inicioProf()throws Exception{
        if((this.getSesion().get("CedulaProfesor")==null)||
                ((String)this.getSesion().get("CedulaProfesor")).equals("")){
            return "sesion_expirada";
        }
        this.setUsuario("");
        this.setPassword("");
        this.setCedula("");
        this.setNombre("");
        return SUCCESS;
    }
    public String iniciarLoginProf()throws Exception{
        this.setUsuario("");
        this.setPassword("");
        return SUCCESS;
    }

    @SuppressWarnings("unchecked")
    public String login() throws Exception {
        GeneralProfDao generalProfDao = new GeneralProfDao();
        System.out.println("Usuario: " + this.getUsuario());
        System.out.println("Password: " + this.getPassword());
        List<ProfesorTo> listProfesorTo = null;
        try {
            listProfesorTo = generalProfDao.consultarProfesores(this.getUsuario(),this.getPassword());
            if(listProfesorTo!=null && listProfesorTo.size()>0){
                this.setCedula(listProfesorTo.get(0).getCedula());
                this.setNombre(listProfesorTo.get(0).getNombre());
                this.getSesion().put("CedulaProfesor",this.getCedula());
                this.getSesion().put("NombreProfesor",this.getNombre());
            }else{
                addActionMessage("No se pudo obtener los datos del usuario...");
                return INPUT;
            }
        } catch (Exception e) {
            e.printStackTrace();
            addActionMessage("Ocurrio un error al intentar acceder a la aplicaccion...\n" + e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }

    public String iniciarIngresarProfesor()throws Exception{
//        if((this.getSesion().get("CedulaProfesor")==null)||
//                ((String)this.getSesion().get("CedulaProfesor")).equals("")){
//            return "sesion_expirada";
//        }
        this.setCedula("");
        this.setNombre("");
        return SUCCESS;
    }

    @SuppressWarnings("unchecked")
    public String ingresarProfesor(){
        GeneralProfDao generalProfDao = new GeneralProfDao();
        List<ProfesorTo> listProfesorTo = null;
        if((this.getSesion().get("CedulaProfesor")==null)||
                ((String)this.getSesion().get("CedulaProfesor")).equals("")){
            return "sesion_expirada";
        }
        System.out.println("Cedula: " + this.getCedula());
        System.out.println("Nombre: " + this.getNombre());
        System.out.println("Password: " + this.getPassword());
        try {
            listProfesorTo = generalProfDao.consultarProfesores(this.getCedula(),"");
            if(listProfesorTo==null || listProfesorTo.size()==0){
                generalProfDao.insertarProfesor(this.getCedula(),this.getNombre(),this.getPassword());
                this.getSesion().put("CedulaProfesor",this.getCedula());
                this.getSesion().put("NombreProfesor",this.getNombre());
            }else{
                addActionMessage("Este usuario ya esta registrado...");
                return INPUT;
            }
        } catch (Exception e) {
            e.printStackTrace();
            addActionMessage("Ocurrio un error al intentar ingresar los datos suminstrados...\n" + e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }
    public String iniciarModificarProfesor()throws Exception{
        if((this.getSesion().get("CedulaProfesor")==null)||
                ((String)this.getSesion().get("CedulaProfesor")).equals("")){
            return "sesion_expirada";
        }
        this.setCedula("");
        this.setNombre("");
        return SUCCESS;
    }

    @SuppressWarnings("unchecked")
    public String modificarProfesor(){
        if((this.getSesion().get("CedulaProfesor")==null)||
                ((String)this.getSesion().get("CedulaProfesor")).equals("")){
            return "sesion_expirada";
        }
        GeneralProfDao generalProfDao = new GeneralProfDao();
        String cedulaActualProfesor = (String)this.getSesion().get("CedulaProfesor");
        System.out.println("Cedula: " + this.getCedula());
        System.out.println("Nombre: " + this.getNombre());
        try {
            generalProfDao.modificarProfesor(cedulaActualProfesor,this.getCedula(),this.getNombre(),this.getPassword());
            this.getSesion().put("CedulaProfesor",this.getCedula());
            this.getSesion().put("NombreProfesor",this.getNombre());
        } catch (Exception e) {
            e.printStackTrace();
            addActionMessage("Ocurrio un error al intentar ingresar los datos suminstrados...\n" + e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }

    public String consultarProfesor() throws Exception {
        if((this.getSesion().get("CedulaProfesor")==null)||
                ((String)this.getSesion().get("CedulaProfesor")).equals("")){
            return "sesion_expirada";
        }
        System.out.println("Cedula: " + (String)this.getSesion().get("CedulaProfesor"));
        System.out.println("Nombre: " + (String)this.getSesion().get("NombreProfesor"));

        this.setCedula((String)this.getSesion().get("CedulaProfesor"));
        this.setNombre((String)this.getSesion().get("NombreProfesor"));
        return SUCCESS;
    }

    @SuppressWarnings("unchecked")
    public String logOutPrf(){
        this.getSesion().put("CedulaProfesor","");
        this.getSesion().put("NombreProfesor","");
        return SUCCESS;
    }

    public String iniciarCrearEvaluacion()throws Exception{
        if((this.getSesion().get("CedulaProfesor")==null)||
                ((String)this.getSesion().get("CedulaProfesor")).equals("")){
            return "sesion_expirada";
        }
        this.setCedula("");
        this.setNombre("");
        this.setCodigo("");
        return SUCCESS;
    }

    public String crearEvaluacion(){
        if((this.getSesion().get("CedulaProfesor")==null)||
                ((String)this.getSesion().get("CedulaProfesor")).equals("")){
            return "sesion_expirada";
        }
        GeneralProfDao generalProfDao = new GeneralProfDao();
        List<EvaluacionTo> listEvaluacionTo = null;
        EvaluacionTo evaluacionTo = null;
        PreguntaTo preguntaTo = null;
        List<PreguntaTo> listPreguntaTo = null;
        this.setCodigo(this.getRequest().getParameter("codigo"));
        try {
            listEvaluacionTo = generalProfDao.consultarEvaluacion(this.getCodigo());
            if(listEvaluacionTo==null || listEvaluacionTo.size()==0){
                evaluacionTo = new EvaluacionTo();
                evaluacionTo.setCodigo(this.getRequest().getParameter("codigo"));
                evaluacionTo.setNombre(this.getRequest().getParameter("nombreEval"));
                listPreguntaTo = new ArrayList<PreguntaTo>();
                for(int ind=1 ; ind<=10 ; ind++){
                    preguntaTo = new PreguntaTo();
                    preguntaTo.setEnunciado(this.getRequest().getParameter("pregunt"+ind));
                    preguntaTo.setResp1(this.getRequest().getParameter("opcion"+ind+"Res1"));
                    preguntaTo.setResp2(this.getRequest().getParameter("opcion"+ind+"Res2"));
                    preguntaTo.setResp3(this.getRequest().getParameter("opcion"+ind+"Res3"));
                    preguntaTo.setResCorrecta(Integer.parseInt(this.getRequest().getParameter("opcionResCorrecta"+ind)));
                    listPreguntaTo.add(preguntaTo);
                }
                evaluacionTo.setPreguntas(listPreguntaTo);
                evaluacionTo.setRespuestas(null);
                generalProfDao.insertarEvaluacion(evaluacionTo);
                generalProfDao.insertarPreguntaEvaluacion(evaluacionTo);
            }else{
                addActionMessage("Esta evaluacion ya esta registrada...");
                return INPUT;
            }
        } catch (Exception e) {
            e.printStackTrace();
            addActionMessage("Ocurrio un error al intentar ingresar los datos suminstrados...\n" + e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }
    public String iniciarConsultarEvaluacion()throws Exception{
        if((this.getSesion().get("CedulaProfesor")==null)||
                ((String)this.getSesion().get("CedulaProfesor")).equals("")){
            return "sesion_expirada";
        }
        this.setCedula("");
        this.setNombre("");
        this.setActivar(false);
        return SUCCESS;
    }

    public String consultarEvaluacion(){
        if((this.getSesion().get("CedulaProfesor")==null)||
                ((String)this.getSesion().get("CedulaProfesor")).equals("")){
            return "sesion_expirada";
        }
        List<RespuestaTo> listRespuestaTo = null;
        List<EstudianteTo> listEstudianteTo = null;
        GeneralProfDao generalProfDao = new GeneralProfDao();
        GeneralEstDao generalEstDao = new GeneralEstDao();
        try{
            listRespuestaTo = generalProfDao.consultarRespuestasEvaluacion((String)this.getRequest().getParameter("codigo"),(String)this.getRequest().getParameter("cedula"));
            if(listRespuestaTo!=null && listRespuestaTo.size()>0){
                listEstudianteTo =  generalEstDao.consultarEstudiantes((String)this.getRequest().getParameter("cedula"),"");
                if(listEstudianteTo!=null && listEstudianteTo.size()>0){
                    this.setListaJSPRespuestasTo(listRespuestaTo);
                    this.setCedula(listEstudianteTo.get(0).getCedula());
                    this.setNombre(listEstudianteTo.get(0).getNombre());
                    this.setSemestre(listEstudianteTo.get(0).getSemestre());
                    this.setNotaGeneral(0);
                    for(int ind = 0 ; ind<listRespuestaTo.size() ; ind++)
                        this.setNotaGeneral(this.getNotaGeneral()+listRespuestaTo.get(ind).getNotaPrg());
                    this.setActivar(true);
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
            addActionMessage("Ocurrio un error al intentar cargar los datos...\n" + e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }
    @SuppressWarnings("unchecked")
    public Map getSesion() {
        return sesion;
    }

    public HttpServletRequest getRequestAction() {
        return request;
    }

    public void setRequestAction(HttpServletRequest request) {
        this.request = request;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @SuppressWarnings("unchecked")
    public void setSession(Map s) {
        this.sesion = s;
    }

    public void setServletRequest(HttpServletRequest request) {
        this.request = request;
    }

    public List<RespuestaTo> getListaJSPRespuestasTo() {
        return listaJSPRespuestasTo;
    }

    public void setListaJSPRespuestasTo(List<RespuestaTo> listaJSPRespuestasTo) {
        this.listaJSPRespuestasTo = listaJSPRespuestasTo;
    }

    public int getNotaGeneral() {
        return notaGeneral;
    }

    public void setNotaGeneral(int notaGeneral) {
        this.notaGeneral = notaGeneral;
    }

    public Boolean isActivar() {
        return activar;
    }

    public void setActivar(Boolean activar) {
        this.activar = activar;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }
}