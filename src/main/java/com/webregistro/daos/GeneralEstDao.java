package com.webregistro.daos;

import com.webregistro.to.RespuestaTo;
import com.webregistro.to.EstudianteTo;
import com.webregistro.to.EvaluacionTo;
import com.webregistro.to.PreguntaTo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: new user
 * Date: 29/11/2011
 * Time: 11:47:40 AM
 * To change this template use File | Settings | File Templates.
 */
public class GeneralEstDao {
    public Connection getConexion() throws Exception {
        Class.forName("org.h2.Driver");
        Connection conexion = DriverManager.getConnection("jdbc:h2:mem:test", "root", "");

//        Class.forName("com.mysql.jdbc.Driver");
//        Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost/geneval", "root", "");
        return conexion;
    }
    public List<EstudianteTo> consultarEstudiantes(String cedula, String password)throws Exception{
        EstudianteTo estudianteTo = null;
        ResultSet rs = null;
        List<EstudianteTo> listEstudianteTo = null;
        if(cedula.equals("") && password.equals("")){
             Connection conexion = this.getConexion();
             Statement st = conexion.createStatement();
             listEstudianteTo = new ArrayList<EstudianteTo>();
             rs  = st.executeQuery("SELECT * FROM estudiantes");
             while(rs.next()){
                 estudianteTo = new EstudianteTo();
                 estudianteTo.setCedula(rs.getString(1));
                 estudianteTo.setNombre(rs.getString(2));
                 estudianteTo.setSemestre(rs.getString(3));
                 listEstudianteTo.add(estudianteTo);
             }
             st.close();
             conexion.close();
         }else{
             if(!cedula.equals("") && !password.equals("")){
                 Connection conexion = this.getConexion();
                 Statement st = conexion.createStatement();
                 listEstudianteTo = new ArrayList<EstudianteTo>();
                 rs = st.executeQuery("SELECT * FROM estudiantes WHERE "+ "id_estudiante="   + "'" + cedula   + "'" + " AND " +
                                                                         "password=" + "'" + password + "'");
                 while(rs.next()){
                     estudianteTo = new EstudianteTo();
                     estudianteTo.setCedula(rs.getString(1));
                     estudianteTo.setNombre(rs.getString(2));
                     estudianteTo.setSemestre(rs.getString(3));
                     listEstudianteTo.add(estudianteTo);
                 }
                 st.close();
                 conexion.close();
             }else{
                 if(!cedula.equals("") && password.equals("")){
                     Connection conexion = this.getConexion();
                     Statement st = conexion.createStatement();
                     listEstudianteTo = new ArrayList<EstudianteTo>();
                     rs = st.executeQuery("SELECT * FROM estudiantes WHERE "+ "id_estudiante="   + "'" + cedula   + "'");
                     while(rs.next()){
                         estudianteTo = new EstudianteTo();
                         estudianteTo.setCedula(rs.getString(1));
                         estudianteTo.setNombre(rs.getString(2));
                         estudianteTo.setSemestre(rs.getString(3));
                         listEstudianteTo.add(estudianteTo);
                     }
                     st.close();
                     conexion.close();
                 }
             }
         }
         return listEstudianteTo;
    }

    public void insertarEstudiante(String cedula, String nombre, String semestre, String password)throws Exception{
        Connection conexion = this.getConexion();
        Statement st = conexion.createStatement();
        st.executeUpdate("INSERT INTO estudiantes VALUES ('"+cedula+   "','"
                                                            +nombre+   "','"
                                                            +semestre+   "','"
                                                            +password+ "'"
                                                            +")");
        st.close();
        conexion.close();
    }
    public void modificarEstudiante(String cedulaActual, String cedula , String nombre, String semestre, String password)throws Exception{
        Connection conexion = this.getConexion();
        Statement st = conexion.createStatement();
        String SQL = "UPDATE estudiantes SET "  + "id_estudiante="   + "'" + cedula         + "'"  + "," +
                                                  "nombre="   + "'" + nombre         + "'"  + "," +
                                                  "semestre=" + "'" + semestre     + "'"  + "," +
                                                  "password=" + "'" + password       + "'"  +
                                      " WHERE " + "id_estudiante="   + "'" + cedulaActual + "'";
        System.out.println(SQL);
        st.executeUpdate(SQL);
        st.close();
        conexion.close();
    }

    public List<EvaluacionTo> consultarEvaluacion(String codigo)throws Exception{
        EvaluacionTo evaluacionTo = null;
        ResultSet rs = null;
        List<EvaluacionTo> listEvaluacionTo = null;
        Connection conexion = this.getConexion();
        Statement st = conexion.createStatement();
        listEvaluacionTo = new ArrayList<EvaluacionTo>();
        rs  = st.executeQuery("SELECT * FROM evaluaciones WHERE "+ "id_evaluacion="   + "'" + codigo   + "'");
        while(rs.next()){
            evaluacionTo = new EvaluacionTo();
            evaluacionTo.setCodigo(rs.getString(1));
            evaluacionTo.setNombre(rs.getString(2));
            evaluacionTo.setFecha(rs.getDate(3));
            listEvaluacionTo.add(evaluacionTo);
        }
        st.close();
        conexion.close();
        return listEvaluacionTo;
    }

    public List<PreguntaTo> consultarPreguntasEvaluacion(String codigo)throws Exception{
        PreguntaTo preguntaTo = null;
        ResultSet rs = null;
        List<PreguntaTo> listPreguntaTo = null;
        Connection conexion = this.getConexion();
        Statement st = conexion.createStatement();
        listPreguntaTo = new ArrayList<PreguntaTo>();
        rs  = st.executeQuery("SELECT * FROM preguntas WHERE "+ "id_evaluacion="   + "'" + codigo   + "'");
        while(rs.next()){
            preguntaTo = new PreguntaTo();
            preguntaTo.setIdPregunta(rs.getInt(1));
            preguntaTo.setEnunciado(rs.getString(3));
            preguntaTo.setResp1(rs.getString(4));
            preguntaTo.setResp2(rs.getString(5));
            preguntaTo.setResp3(rs.getString(6));
            preguntaTo.setResCorrecta(rs.getInt(7));
            listPreguntaTo.add(preguntaTo);
        }
        st.close();
        conexion.close();
        return listPreguntaTo;
    }

    public void insertarEvaluacion(EvaluacionTo evaluacionTo)throws Exception{
        Connection conexion = this.getConexion();
        Statement st = conexion.createStatement();
        String SQL =  "";
        for(int ind = 0 ; ind < evaluacionTo.getRespuestas().size(); ind++){
            SQL = "INSERT INTO respuestas(" +
                    "id_pregunta," +
                    "id_evaluacion," +
                    "id_estudiante," +
                    "numero_respuesta," +
                    "nota_resp" +
                    ")" +
                    "VALUES (" + "'" + evaluacionTo.getPreguntas().get(ind).getIdPregunta() + "'," +
                                 "'" + evaluacionTo.getCodigo() + "'," +
                                 "'" + evaluacionTo.getRespuestas().get(ind).getId_estudiante() + "'," +
                                 "'" + evaluacionTo.getRespuestas().get(ind).getNumero_respuesta() + "'," +
                                 "'" + evaluacionTo.getRespuestas().get(ind).getNotaPrg() + "'" +
                    ")";
            st.executeUpdate(SQL);
        }
        st.close();
        conexion.close();
    }
    public List<RespuestaTo> consultarRespuestasEvaluacion(String codigo, String cedula)throws Exception{
        RespuestaTo respuestaTo = null;
        ResultSet rs = null;
        List<RespuestaTo> listRespuestaTo = null;
        Connection conexion = this.getConexion();
        Statement st = conexion.createStatement();
        listRespuestaTo = new ArrayList<RespuestaTo>();
        rs  = st.executeQuery("SELECT nota_resp FROM respuestas WHERE "+ "id_evaluacion="   + "'" + codigo + " 'AND " +
                                                                 "id_estudiante="   + "'" + cedula +
                                                                 "'");
        while(rs.next()){
            respuestaTo = new RespuestaTo();
            respuestaTo.setNotaPrg(rs.getInt(1));
            listRespuestaTo.add(respuestaTo);
        }
        st.close();
        conexion.close();
        return listRespuestaTo;
    }
}
