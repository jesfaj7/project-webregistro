package com.webregistro.daos;

import com.webregistro.to.ProfesorTo;
import com.webregistro.to.RespuestaTo;
import com.webregistro.to.EvaluacionTo;

import java.sql.*;
import java.util.List;
import java.util.ArrayList;
import java.text.SimpleDateFormat;


/**
 * Created by IntelliJ IDEA.
 * User: new user
 * Date: 29/11/2011
 * Time: 11:47:40 AM
 * To change this template use File | Settings | File Templates.
 */
public class GeneralProfDao {
    public Connection getConexion() throws Exception {
        Class.forName("org.h2.Driver");
        Connection conexion = DriverManager.getConnection("jdbc:h2:mem:test", "root", "");

//        Class.forName("com.mysql.jdbc.Driver");
//        Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost/geneval", "root", "");
        return conexion;
    }
    public List<ProfesorTo> consultarProfesores(String cedula, String password)throws Exception{
        ProfesorTo profesorTo = null;
        ResultSet rs = null;
        List<ProfesorTo> listProfesorTo = null;
        if(cedula.equals("") && password.equals("")){
             Connection conexion = this.getConexion();
             Statement st = conexion.createStatement();
             listProfesorTo = new ArrayList<ProfesorTo>();
             rs  = st.executeQuery("SELECT * FROM profesores");
             while(rs.next()){
                 profesorTo = new ProfesorTo();
                 profesorTo.setCedula(rs.getString(1));
                 profesorTo.setNombre(rs.getString(2));
                 listProfesorTo.add(profesorTo);
             }
             st.close();
             conexion.close();
         }else{
             if(!cedula.equals("") && !password.equals("")){
                 Connection conexion = this.getConexion();
                 Statement st = conexion.createStatement();
                 listProfesorTo = new ArrayList<ProfesorTo>();
                 rs = st.executeQuery("SELECT * FROM profesores WHERE "+ "id_profesor="   + "'" + cedula   + "'" + " AND " +
                                                                         "password=" + "'" + password + "'");
                 while(rs.next()){
                    profesorTo = new ProfesorTo();
                    profesorTo.setCedula(rs.getString(1));
                    profesorTo.setNombre(rs.getString(2));
                    listProfesorTo.add(profesorTo);
                 }
                 st.close();
                 conexion.close();
             }else{
                 if(!cedula.equals("") && password.equals("")){
                     Connection conexion = this.getConexion();
                     Statement st = conexion.createStatement();
                     listProfesorTo = new ArrayList<ProfesorTo>();
                     rs = st.executeQuery("SELECT * FROM profesores WHERE "+ "id_profesor="   + "'" + cedula   + "'");
                     while(rs.next()){
                        profesorTo = new ProfesorTo();
                        profesorTo.setCedula(rs.getString(1));
                        profesorTo.setNombre(rs.getString(2));
                        listProfesorTo.add(profesorTo);
                     }
                     st.close();
                     conexion.close();
                 }
             }
         }
         return listProfesorTo;
    }

    public void insertarProfesor(String cedula, String nombre, String password)throws Exception{
        Connection conexion = this.getConexion();
        Statement st = conexion.createStatement();
        st.executeUpdate("INSERT INTO profesores VALUES ('"+cedula+   "','"
                                                           +nombre+   "','"
                                                           +password+ "'"
                                                           +")");
        st.close();
        conexion.close();
    }
    public void modificarProfesor(String cedulaActual, String cedula , String nombre, String password)throws Exception{
        Connection conexion = this.getConexion();
        Statement st = conexion.createStatement();
        String SQL = "UPDATE profesores SET "  +  "id_profesor="   + "'" + cedula         + "'"  + "," +
                                                  "nombre="   + "'" + nombre         + "'"  + "," +
                                                  "password=" + "'" + password       + "'"  +
                                      " WHERE " + "id_profesor="   + "'" + cedulaActual + "'";
        System.out.println(SQL);
        st.executeUpdate(SQL);
        st.close();
        conexion.close();
    }
    public void insertarEvaluacion(EvaluacionTo evaluacionTo)throws Exception{
        Connection conexion = this.getConexion();
        Statement st = conexion.createStatement();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        st.executeUpdate("INSERT INTO evaluaciones VALUES ('"+evaluacionTo.getCodigo()+   "','"
                                                             +evaluacionTo.getNombre()+   "','"
                                                             +sdf.format(new java.util.Date())
                                                             +"')");
        st.close();
        conexion.close();
    }
    public void insertarPreguntaEvaluacion(EvaluacionTo evaluacionTo)throws Exception{
        Connection conexion = this.getConexion();
        Statement st = conexion.createStatement();
        for(int ind = 0 ; ind<evaluacionTo.getPreguntas().size() ;ind++){
            st.executeUpdate("INSERT INTO preguntas(id_evaluacion,enunciado,resp1,resp2,resp3,resp_cor) " +
               "                VALUES ('"+evaluacionTo.getCodigo()+   "','"
                                          +evaluacionTo.getPreguntas().get(ind).getEnunciado()+   "','"
                                          +evaluacionTo.getPreguntas().get(ind).getResp1()+   "','"
                                          +evaluacionTo.getPreguntas().get(ind).getResp2()+   "','"
                                          +evaluacionTo.getPreguntas().get(ind).getResp3()+   "','"
                                          +evaluacionTo.getPreguntas().get(ind).getResCorrecta()+ "'"
                                          +")");
        }
        st.close();
        conexion.close();
    }
    public List<EvaluacionTo> consultarEvaluacion(String codigo)throws Exception{
        EvaluacionTo evaluacionTo = null;
        ResultSet rs = null;
        List<EvaluacionTo> listEvaluacionTo = null;
        Connection conexion = this.getConexion();
        Statement st = conexion.createStatement();
        listEvaluacionTo = new ArrayList<EvaluacionTo>();
        rs  = st.executeQuery("SELECT * FROM evaluaciones WHERE "+ "id_evaluacion="   + "'" + codigo   + "'");
        while(rs.next()){
            evaluacionTo = new EvaluacionTo();
            evaluacionTo.setCodigo(rs.getString(1));
            evaluacionTo.setNombre(rs.getString(2));
            evaluacionTo.setFecha(rs.getDate(3));
            listEvaluacionTo.add(evaluacionTo);
        }
        st.close();
        conexion.close();
        return listEvaluacionTo;
    }
    public List<RespuestaTo> consultarRespuestasEvaluacion(String codigo, String cedula)throws Exception{
        RespuestaTo respuestaTo = null;
        ResultSet rs = null;
        List<RespuestaTo> listRespuestaTo = null;
        Connection conexion = this.getConexion();
        Statement st = conexion.createStatement();
        listRespuestaTo = new ArrayList<RespuestaTo>();
        rs  = st.executeQuery("SELECT nota_resp FROM respuestas WHERE "+ "id_evaluacion="   + "'" + codigo + " 'AND " +
                                                                 "id_estudiante="   + "'" + cedula +
                                                                 "'");
        while(rs.next()){
            respuestaTo = new RespuestaTo();
            respuestaTo.setNotaPrg(rs.getInt(1));
            listRespuestaTo.add(respuestaTo);
        }
        st.close();
        conexion.close();
        return listRespuestaTo;
    }
}