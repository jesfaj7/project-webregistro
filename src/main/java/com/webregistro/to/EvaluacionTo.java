package com.webregistro.to;

import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: new user
 * Date: 30/11/2011
 * Time: 10:55:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class EvaluacionTo {
    private String codigo;
    private String nombre;
    private Date fecha;
    private List<PreguntaTo> preguntas;
    private List<RespuestaTo> respuestas;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public List<PreguntaTo> getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(List<PreguntaTo> preguntas) {
        this.preguntas = preguntas;
    }

    public List<RespuestaTo> getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(List<RespuestaTo> respuestas) {
        this.respuestas = respuestas;
    }
}
