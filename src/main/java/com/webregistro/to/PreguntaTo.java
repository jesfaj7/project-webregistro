package com.webregistro.to;

/**
 * Created by IntelliJ IDEA.
 * User: new user
 * Date: 30/11/2011
 * Time: 10:55:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class PreguntaTo {
    private int idPregunta;
    private String enunciado;
    private String resp1;
    private String resp2;
    private String resp3;
    private int resCorrecta;

    public int getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(int idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getEnunciado() {
        return enunciado;
    }

    public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
    }

    public String getResp1() {
        return resp1;
    }

    public void setResp1(String resp1) {
        this.resp1 = resp1;
    }

    public String getResp2() {
        return resp2;
    }

    public void setResp2(String resp2) {
        this.resp2 = resp2;
    }

    public String getResp3() {
        return resp3;
    }

    public void setResp3(String resp3) {
        this.resp3 = resp3;
    }

    public int getResCorrecta() {
        return resCorrecta;
    }

    public void setResCorrecta(int resCorrecta) {
        this.resCorrecta = resCorrecta;
    }
}
