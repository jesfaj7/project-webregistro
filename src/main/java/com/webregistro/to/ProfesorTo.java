package com.webregistro.to;

/**
 * Created by IntelliJ IDEA.
 * User: new user
 * Date: 29/11/2011
 * Time: 03:52:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProfesorTo {
    private String password;
    private String cedula;
    private String nombre;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
