package com.webregistro.to;

/**
 * Created by IntelliJ IDEA.
 * User: new user
 * Date: 30/11/2011
 * Time: 10:55:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class RespuestaTo {
    private int id_respuesta;
    private int id_pregunta;
    private String id_evaluacion;
    private String id_estudiante;
    private int numero_respuesta;
    private int notaPrg;

    public int getId_respuesta() {
        return id_respuesta;
    }

    public void setId_respuesta(int id_respuesta) {
        this.id_respuesta = id_respuesta;
    }

    public int getId_pregunta() {
        return id_pregunta;
    }

    public void setId_pregunta(int id_pregunta) {
        this.id_pregunta = id_pregunta;
    }

    public String getId_evaluacion() {
        return id_evaluacion;
    }

    public void setId_evaluacion(String id_evaluacion) {
        this.id_evaluacion = id_evaluacion;
    }

    public String getId_estudiante() {
        return id_estudiante;
    }

    public void setId_estudiante(String id_estudiante) {
        this.id_estudiante = id_estudiante;
    }

    public int getNumero_respuesta() {
        return numero_respuesta;
    }

    public void setNumero_respuesta(int numero_respuesta) {
        this.numero_respuesta = numero_respuesta;
    }

    public int getNotaPrg() {
        return notaPrg;
    }

    public void setNotaPrg(int notaPrg) {
        this.notaPrg = notaPrg;
    }
}
