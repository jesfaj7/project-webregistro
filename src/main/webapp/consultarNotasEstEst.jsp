<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US" xml:lang="en">
<head>
    <!--
    Created by Artisteer v3.0.0.32906
    Base template (without user's data) checked by http://validator.w3.org : "This page is valid XHTML 1.0 Transitional"
    -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Sistema Generador de Pruebas</title>

    <link rel="stylesheet" href="style.css" type="text/css" media="screen" />
    <!--[if IE 6]><link rel="stylesheet" href="style.ie6.css" type="text/css" media="screen" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" href="style.ie7.css" type="text/css" media="screen" /><![endif]-->

    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="script.js"></script>
</head>
<body>
<div id="art-page-background-glare">
        <div id="art-page-background-glare-image">
    <div id="art-main">
        <div class="art-sheet">
            <div class="art-sheet-tl"></div>
            <div class="art-sheet-tr"></div>
            <div class="art-sheet-bl"></div>
            <div class="art-sheet-br"></div>
            <div class="art-sheet-tc"></div>
            <div class="art-sheet-bc"></div>
            <div class="art-sheet-cl"></div>
            <div class="art-sheet-cr"></div>
            <div class="art-sheet-cc"></div>
            <div class="art-sheet-body">
                <div class="art-nav">
                	<div class="l"></div>
                	<div class="r"></div>
                	<ul class="art-menu">
                		<li>
                            <script language="JavaScript">
                                 function enviarInicio(){
                                     var formulario = document.forms[0];
                                     formulario.action = "inicioEst.action";
                                     formulario.submit();
                                 }
                            </script>
                			<a href="#" onclick="js:enviarInicio();" class="active"><span class="l"></span><span class="r"></span><span class="t">Inicio</span></a>
                		</li>
                		<li>
                			<a href="#"><span class="l"></span><span class="r"></span><span class="t">Menu Principal</span></a>
                			<ul>
                				<li><a href="#">Gestion de Datos Estudiante</a>
                					<ul>
                                        <script language="JavaScript">
                                             function enviarConsulta(){
                                                 var formulario = document.forms[0];
                                                 formulario.action = "consultarEstudiante.action";
                                                 formulario.submit();
                                             }
                                             function enviarModificacion(){
                                                 var formulario = document.forms[0];
                                                 formulario.action = "iniciarModificarEstudiante.action";
                                                 formulario.submit();
                                             }
                                        </script>
                						<li><a href="#" onclick="js:enviarConsulta();">Consultar Datos Personales</a></li>
                                        <li><a href="#" onclick="js:enviarModificacion();">Modificar Datos Personales</a></li>
                					</ul>
                				</li>
                			</ul>
                		</li>
                		<li>
                			<a href="acercaDeEst.jsp"><span class="l"></span><span class="r"></span><span class="t">Acerca de...</span></a>
                		</li>
                	</ul>
                </div>
                <div class="art-content-layout">
                    <div class="art-content-layout-row">
                        <div class="art-layout-cell art-sidebar1">
                          <div class="art-vmenublock">
                              <div class="art-vmenublock-body">
                                          <div class="art-vmenublockheader">
                                              <h3 class="t">Gestion de Evaluaciones</h3>
                                          </div>
                                          <div class="art-vmenublockcontent">
                                              <div class="art-vmenublockcontent-body">
                                                          <ul class="art-vmenu">
                                                              <script language="JavaScript">
                                                                   function enviarPresentar(){
                                                                       var formulario = document.forms[0];
                                                                       formulario.action = "iniciarPresentarEvaluacion.action";
                                                                       formulario.submit();
                                                                   }
                                                                   function enviarVerNota(){
                                                                       var formulario = document.forms[0];
                                                                       formulario.action = "iniciarConsultarEvaluacion.action";
                                                                       formulario.submit();
                                                                   }
                                                                   function enviarLogout(){
                                                                       var formulario = document.forms[0];
                                                                       formulario.action = "logOut.action";
                                                                       formulario.submit();
                                                                   }
                                                              </script>
                                                              <li>
                                                                  <a href="#" onclick="js:enviarPresentar();"><span class="l"></span><span class="r"></span><span class="t">Presentar Evaluacion</span></a>
                                                              </li>
                                                              <li>
                                                                  <a href="#" onclick="js:enviarVerNota();"><span class="l"></span><span class="r"></span><span class="t">Consultar Evaluacion</span></a>
                                                              </li>
                                                              <li>
                                                                  <a href="#" onclick="js:enviarLogout();"><span class="l"></span><span class="r"></span><span class="t">Logout</span></a>
                                                              </li>
                                                          </ul>

                                          		<div class="cleared"></div>
                                              </div>
                                          </div>
                          		<div class="cleared"></div>
                              </div>
                          </div>
                          <div class="cleared"></div>
                        </div>
                        <div class="art-layout-cell art-content">
                          <div class="art-post">
                              <div class="art-post-body">
                          <div class="art-post-inner art-article">
                              <h2 class="art-postheader">Sistema Generador de Evaluaciones</h2>
                                          <div class="art-postcontent">
                                              <img src="joomla_images/Joomla.png" alt="an image" style="float:left;border:0;margin: 1em 1em 0 0;" />
                                              <p>Software Educativo para el estudio y gestion de evaluaciones academicas.</p>
                                              <br>
                                              <br>
                                              <br>
                                              <div class="art-blockheader">
                                                  <h3 class="t">Consultar Notas Evaluacion</h3>
                                              </div>
                                              <form action="consultarEvaluacion.action" method="post" name="form" id="form" >
                                               <fieldset class="input">
                                               <p>
                                               <label>Codigo:</label><input type="text" name="codigo" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <span class="art-button-wrapper">
                                               <span class="art-button-l"> </span>
                                               <span class="art-button-r"> </span>
                                               <input type="submit" name="Submit" class="art-button" value="Ver Notas" />
                                               </span>
                                               <s:if test="Activar">
                                                  <p>Cedula: <s:property value="cedula"/></p>
                                                  <p>Nombre: <s:property value="nombre"/></p>
                                                  <p>Semestre: <s:property value="semestre"/></p>
                                                  <p>Nota General: <s:property value="notaGeneral"/></p>
                                                  <%int indentificador = 1;%>
                                                  <s:iterator id="respuestas" value="ListaJSPRespuestasTo" status="it">
                                                      <p>Nota Pregunta <%=indentificador%>: <s:property value="notaPrg"/> </p>
                                                      <%indentificador++;%>
                                                  </s:iterator>
                                               </s:if>
                                               <br>
                                               </fieldset>
                                              </form>
                                          </div>
                                          <div class="cleared"></div>
                          </div>
                          		<div class="cleared"></div>
                              </div>
                          </div>
                          <div class="art-post">
                              <div class="art-post-body">
                          		<div class="cleared"></div>
                              </div>
                          </div>
                          <div class="cleared"></div>
                        </div>
                    </div>
                </div>
                <div class="cleared"></div>
                <div class="art-footer">
                    <div class="art-footer-t"></div>
                    <div class="art-footer-l"></div>
                    <div class="art-footer-b"></div>
                    <div class="art-footer-r"></div>
                    <div class="art-footer-body">
                        <div class="art-footer-text">
                            <p>Copyright &copy; 2011. All Rights Reserved.</p>
                        </div>
                		<div class="cleared"></div>
                    </div>
                </div>
        		<div class="cleared"></div>
            </div>
        </div>
    </div>
        </div>
    </div>





</body>
</html>
