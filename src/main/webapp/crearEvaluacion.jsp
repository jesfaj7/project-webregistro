<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US" xml:lang="en">
<head>
    <!--
    Created by Artisteer v3.0.0.32906
    Base template (without user's data) checked by http://validator.w3.org : "This page is valid XHTML 1.0 Transitional"
    -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Sistema Generador de Pruebas</title>

    <link rel="stylesheet" href="<s:url value="style.css"/>" type="text/css" media="screen" />
    <!--[if IE 6]><link rel="stylesheet" href="style.ie6.css" type="text/css" media="screen" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" href="style.ie7.css" type="text/css" media="screen" /><![endif]-->

    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="script.js"></script>
</head>
<body>
<div id="art-page-background-glare">
        <div id="art-page-background-glare-image">
    <div id="art-main">
        <div class="art-sheet">
            <div class="art-sheet-tl"></div>
            <div class="art-sheet-tr"></div>
            <div class="art-sheet-bl"></div>
            <div class="art-sheet-br"></div>
            <div class="art-sheet-tc"></div>
            <div class="art-sheet-bc"></div>
            <div class="art-sheet-cl"></div>
            <div class="art-sheet-cr"></div>
            <div class="art-sheet-cc"></div>
            <div class="art-sheet-body">
                <div class="art-nav">
                	<div class="l"></div>
                	<div class="r"></div>
                	<ul class="art-menu">
                		<li>
                            <script language="JavaScript">
                                 function enviarInicio(){
                                     var formulario = document.forms[0];
                                     formulario.action = "inicioProf.action";
                                     formulario.submit();
                                 }
                            </script>
                			<a href="#" onclick="js:enviarInicio();" class="active"><span class="l"></span><span class="r"></span><span class="t">Inicio</span></a>
                		</li>
                		<li>
                			<a href="#"><span class="l"></span><span class="r"></span><span class="t">Menu Principal</span></a>
                			<ul>
                				<li><a href="#">Gestion de Datos Profesor</a>
                					<ul>
                                        <script language="JavaScript">
                                             function enviarConsulta(){
                                                 var formulario = document.forms[0];
                                                 formulario.action = "consultarProfesor.action";
                                                 formulario.submit();
                                             }
                                             function enviarModificacion(){
                                                 var formulario = document.forms[0];
                                                 formulario.action = "iniciarModificarProfesor.action";
                                                 formulario.submit();
                                             }
                                        </script>
                						<li><a href="#" onclick="js:enviarConsulta();">Consultar Datos Personales</a></li>
                                        <li><a href="#" onclick="js:enviarModificacion();">Modificar Datos Personales</a></li>
                					</ul>
                				</li>
                			</ul>
                		</li>		
                		<li>
                			<a href="acercaDePrf.jsp"><span class="l"></span><span class="r"></span><span class="t">Acerca de...</span></a>
                		</li>
                	</ul>
                </div>
                <div class="art-content-layout">
                    <div class="art-content-layout-row">
                        <div class="art-layout-cell art-sidebar1">
                          <div class="art-vmenublock">
                              <div class="art-vmenublock-body">
                                          <div class="art-vmenublockheader">
                                              <h3 class="t">Gestion de Evaluaciones</h3>
                                          </div>
                                          <div class="art-vmenublockcontent">
                                              <div class="art-vmenublockcontent-body">
                                                          <ul class="art-vmenu">
                                                              <script language="JavaScript">
                                                                  function enviarCrear(){
                                                                      var formulario = document.forms[0];
                                                                      formulario.action = "iniciarCrearEvaluacion.action";
                                                                      formulario.submit();
                                                                  }
                                                                  function enviarVerNota(){
                                                                      var formulario = document.forms[0];
                                                                      formulario.action = "iniciarConsultarEvaluacionPrf.action";
                                                                      formulario.submit();
                                                                  }
                                                                  function enviarLogout(){
                                                                      var formulario = document.forms[0];
                                                                      formulario.action = "logOutPrf.action";
                                                                      formulario.submit();
                                                                  }
                                                             </script>
                                                             <li>
                                                                 <a href="#" onclick="js:enviarCrear();"><span class="l"></span><span class="r"></span><span class="t">Crear Evaluacion</span></a>
                                                             </li>
                                                             <li>
                                                                 <a href="#" onclick="js:enviarVerNota();"><span class="l"></span><span class="r"></span><span class="t">Consultar Nota Estudiante</span></a>
                                                             </li>
                                                             <li>
                                                                 <a href="#" onclick="js:enviarLogout();"><span class="l"></span><span class="r"></span><span class="t">Logout</span></a>
                                                             </li>
                                                          </ul>
                                          
                                          		<div class="cleared"></div>
                                              </div>
                                          </div>
                          		<div class="cleared"></div>
                              </div>
                          </div>
                          <div class="cleared"></div>
                        </div>
                        <div class="art-layout-cell art-content">
                          <div class="art-post">
                              <div class="art-post-body">
                          <div class="art-post-inner art-article">
                                          <h2 class="art-postheader">Sistema Generador de Evaluaciones</h2>
                                          <div class="art-postcontent">
                                                  <img src="joomla_images/Joomla.png" alt="an image" style="float:left;border:0;margin: 1em 1em 0 0;" />
                                                  <p>Para crear una evaluacion debe llenar todas las casillas. La prueba generada sera 
												  de seleccion simple.</p>
												  <br><br>
                                          <div class="art-blockheader">
                                              <h3 class="t">Ingresar Datos Evaluacion</h3>
                                          </div>
                                          <div class="art-postcontent">
											  <form action="crearEvaluacion.action" method="post" name="crearPrueba" id="form" >
											   <fieldset class="input">
                                               <p>
                                               <label>Codigo:</label><input type="text" name="codigo" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Nombre Evaluacion:</label><input type="text" name="nombreEval" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Pregunta 1:</label><input type="text" name="pregunt1" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 1:</label><input type="text" name="opcion1Res1" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 2:</label><input type="text" name="opcion1Res2" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 3:</label><input type="text" name="opcion1Res3" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta Correcta:</label><select name="opcionResCorrecta1" >
                                               <option value="1">1</option>
                                               <option value="2">2</option>
                                               <option value="3">3</option>
                                               </select>
                                               </p>
                                               <br>
                                               <p>
                                               <label>Pregunta 2:</label><input type="text" name="pregunt2" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 1:</label><input type="text" name="opcion2Res1" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 2:</label><input type="text" name="opcion2Res2" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 3:</label><input type="text" name="opcion2Res3" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta Correcta:</label><select name="opcionResCorrecta2" >
                                               <option value="1">1</option>
                                               <option value="2">2</option>
                                               <option value="3">3</option>
                                               </select>
                                               </p>
                                               <br>
                                               <p>
                                               <label>Pregunta 3:</label><input type="text" name="pregunt3" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 1:</label><input type="text" name="opcion3Res1" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 2:</label><input type="text" name="opcion3Res2" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 3:</label><input type="text" name="opcion3Res3" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta Correcta:</label><select name="opcionResCorrecta3" >
                                               <option value="1">1</option>
                                               <option value="2">2</option>
                                               <option value="3">3</option>
                                               </select>
                                               </p>
                                               <br>
                                               <p>
                                               <label>Pregunta 4:</label><input type="text" name="pregunt4" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 1:</label><input type="text" name="opcion4Res1" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 2:</label><input type="text" name="opcion4Res2" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 3:</label><input type="text" name="opcion4Res3" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta Correcta:</label><select name="opcionResCorrecta4" >
                                               <option value="1">1</option>
                                               <option value="2">2</option>
                                               <option value="3">3</option>
                                               </select>
                                               </p>
                                               <br>
                                               <p>
                                               <label>Pregunta 5:</label><input type="text" name="pregunt5" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 1:</label><input type="text" name="opcion5Res1" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 2:</label><input type="text" name="opcion5Res2" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 3:</label><input type="text" name="opcion5Res3" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta Correcta:</label><select name="opcionResCorrecta5" >
                                               <option value="1">1</option>
                                               <option value="2">2</option>
                                               <option value="3">3</option>
                                               </select>
                                               </p>
                                               <br>
                                               <p>
                                               <label>Pregunta 6:</label><input type="text" name="pregunt6" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 1:</label><input type="text" name="opcion6Res1" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 2:</label><input type="text" name="opcion6Res2" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 3:</label><input type="text" name="opcion6Res3" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta Correcta:</label><select name="opcionResCorrecta6" >
                                               <option value="1">1</option>
                                               <option value="2">2</option>
                                               <option value="3">3</option>
                                               </select>
                                               </p>
                                               <br>
                                               <p>
                                               <label>Pregunta 7:</label><input type="text" name="pregunt7" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 1:</label><input type="text" name="opcion7Res1" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 2:</label><input type="text" name="opcion7Res2" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 3:</label><input type="text" name="opcion7Res3" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta Correcta:</label><select name="opcionResCorrecta7" >
                                               <option value="1">1</option>
                                               <option value="2">2</option>
                                               <option value="3">3</option>
                                               </select>
                                               </p>
                                               <br>
                                               <p>
                                               <label>Pregunta 8:</label><input type="text" name="pregunt8" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 1:</label><input type="text" name="opcion8Res1" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 2:</label><input type="text" name="opcion8Res2" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 3:</label><input type="text" name="opcion8Res3" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta Correcta:</label><select name="opcionResCorrecta8" >
                                               <option value="1">1</option>
                                               <option value="2">2</option>
                                               <option value="3">3</option>
                                               </select>
                                               </p>
                                               <br>
                                               <p>
                                               <label>Pregunta 9:</label><input type="text" name="pregunt9" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 1:</label><input type="text" name="opcion9Res1" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 2:</label><input type="text" name="opcion9Res2" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 3:</label><input type="text" name="opcion9Res3" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta Correcta:</label><select name="opcionResCorrecta9" >
                                               <option value="1">1</option>
                                               <option value="2">2</option>
                                               <option value="3">3</option>
                                               </select>
                                               </p>
                                               <br>
                                               <p>
                                               <label>Pregunta 10:</label><input type="text" name="pregunt10" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 1:</label><input type="text" name="opcion10Res1" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 2:</label><input type="text" name="opcion10Res2" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta 3:</label><input type="text" name="opcion10Res3" style="border: 1px solid #ccc; width: 30%;"/>
                                               </p>
                                               <p>
                                               <label>Respuesta Correcta:</label><select name="opcionResCorrecta10" >
                                               <option value="1">1</option>
                                               <option value="2">2</option>
                                               <option value="3">3</option>
                                               </select>
                                               </p>
                                               <br>
                                               <span class="art-button-wrapper">
                                               <span class="art-button-l"> </span>
                                               <span class="art-button-r"> </span>
                                               <input type="submit" name="Submit" class="art-button" value="Ingresar" />
                                               </span>
											   </fieldset>
											  </form>
                                          </div>
                                          <div class="cleared"></div>
                                          </div>
                                          <div class="cleared"></div>
                          </div>
                          		<div class="cleared"></div>
                              </div>
                          </div>
                          <div class="art-post">
                              <div class="art-post-body">
                          		<div class="cleared"></div>
                              </div>
                          </div>
                          <div class="cleared"></div>
                        </div>
                    </div>
                </div>
                <div class="cleared"></div>
                <div class="art-footer">
                    <div class="art-footer-t"></div>
                    <div class="art-footer-l"></div>
                    <div class="art-footer-b"></div>
                    <div class="art-footer-r"></div>
                    <div class="art-footer-body">
                        <div class="art-footer-text">
                            <p>Copyright &copy; 2011. All Rights Reserved.</p>
                        </div>
                		<div class="cleared"></div>
                    </div>
                </div>
        		<div class="cleared"></div>
            </div>
        </div>
    </div>
        </div>
    </div>
    
    
    
    
    
</body>
</html>
