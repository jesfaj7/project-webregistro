<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US" xml:lang="en">
<head>
    <!--
    Created by Artisteer v3.0.0.32906
    Base template (without user's data) checked by http://validator.w3.org : "This page is valid XHTML 1.0 Transitional"
    -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Sistema Generador de Pruebas</title>
    <link rel="stylesheet" href="<s:url value="style.css"/>" type="text/css" media="screen" />
    <!--[if IE 6]><link rel="stylesheet" href="style.ie6.css" type="text/css" media="screen" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" href="style.ie7.css" type="text/css" media="screen" /><![endif]-->

    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="script.js"></script>
</head>
<body>
<div id="art-page-background-glare">
        <div id="art-page-background-glare-image">
    <div id="art-main">
        <div class="art-sheet">
            <div class="art-sheet-tl"></div>
            <div class="art-sheet-tr"></div>
            <div class="art-sheet-bl"></div>
            <div class="art-sheet-br"></div>
            <div class="art-sheet-tc"></div>
            <div class="art-sheet-bc"></div>
            <div class="art-sheet-cl"></div>
            <div class="art-sheet-cr"></div>
            <div class="art-sheet-cc"></div>
            <div class="art-sheet-body">
                <div class="art-nav">
                	<div class="l"></div>
                	<div class="r"></div>
                </div>
                <div class="art-content-layout">
                    <div class="art-content-layout-row">
                        <div class="art-layout-cell art-sidebar1">
                          <div class="art-vmenublock">
                              <div class="art-vmenublock-body">
                          		<div class="cleared"></div>
                              </div>
                          </div>
                          <div class="art-block">
                              <div class="art-block-body">
                                          <div class="art-blockheader">
                                              <h3 class="t">Login</h3>
                                          </div>
                                          <div class="art-blockcontent">
                                              <div class="art-blockcontent-body">
                                                          <form action="loginEst.action" method="post" name="login" id="form-login" >
                                                           <fieldset class="input">
                                                           <p id="form-login-username">
                                                            <label>Usuario</label><br/>
                                                            <s:textfield cssClass="inputbox" id="username" name="usuario"/>
                                                           </p>
                                                           <p id="form-login-password">
                                                               <label>Password</label><br/>
                                                               <s:password cssClass="inputbox" id="passwd" name="password"/>
                                                           </p>
                                                           <p id="form-login-mensaje">
                                                               <s:actionmessage/>
                                                           </p>
                                                        	<span class="art-button-wrapper">
                                                          		<span class="art-button-l"> </span>
                                                          		<span class="art-button-r"> </span>
                                                          		<input type="submit" name="Submit" class="art-button" value="Login" />
                                                          	</span>
                                                           </fieldset>
                                                           <ul>
                                                               <script language="JavaScript">
                                                                    function enviar(){
                                                                        var formulario = document.forms[0];
                                                                        formulario.action = "iniciarIngresarEstudiante.action";
                                                                        formulario.submit();
                                                                    }
                                                               </script>
                                                               <li><a href="#" onclick="js:enviar();">Crear Cuenta</a></li>
                                                           </ul>
                                                          </form>
                                          
                                          		<div class="cleared"></div>
                                              </div>
                                          </div>
                          		<div class="cleared"></div>
                              </div>
                          </div>
                          <div class="cleared"></div>
                        </div>
                        <div class="art-layout-cell art-content">
                          <div class="art-post">
                              <div class="art-post-body">
                          <div class="art-post-inner art-article">
                                          <h2 class="art-postheader">Sistema Generador de Evaluaciones</h2>
                                          <div class="art-postcontent">
                                                  <img src="joomla_images/Joomla.png" alt="an image" style="float:left;border:0;margin: 1em 1em 0 0;" />
                                                  <p>Software Educativo para el estudio y gestion de evaluaciones academicas.</p>
                                          </div>
                                          <div class="cleared"></div>
                          </div>
                          		<div class="cleared"></div>
                              </div>
                          </div>
                          <div class="art-post">
                              <div class="art-post-body">
                          		<div class="cleared"></div>
                              </div>
                          </div>
                          <div class="cleared"></div>
                        </div>
                    </div>
                </div>
                <div class="cleared"></div>
                <div class="art-footer">
                    <div class="art-footer-t"></div>
                    <div class="art-footer-l"></div>
                    <div class="art-footer-b"></div>
                    <div class="art-footer-r"></div>
                    <div class="art-footer-body">
                        <div class="art-footer-text">
                            <p>Copyright &copy; 2011. All Rights Reserved.</p>
                        </div>
                		<div class="cleared"></div>
                    </div>
                </div>
        		<div class="cleared"></div>
            </div>
        </div>
    </div>
        </div>
    </div>
    
    
    
    
    
</body>
</html>
